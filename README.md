## Notabene Widget NextJS Example

### Usage

```
npm install
# or
yarn
```

Copy the .env.local.example file to .env.local and add all necessary env vars.

```
API_URL=
API_TOKEN=
VASP_DID=
WIDGET_BASE=
```

Run the development server:

```bash◊
npm run dev
# or
yarn dev
```
