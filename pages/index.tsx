import Head from "next/head";
import styles from "../styles/Home.module.css";
import Widget from "../src/Widget";
import { getCustomerToken } from "../src/utils";
import Modal from "../src/Modal";
import { useState } from "react";

const defaultTransaction = {
	transactionAmount: "15000000000",
	transactionAsset: "xrp",
	beneficiaryAccountNumber: "0x123456789",
};

const Home = ({
	customerToken,
	vaspDID,
	widgetBaseURL,
}: {
	customerToken: string;
	vaspDID: string;
	widgetBaseURL: string;
}) => {
	const [isOpen, setIsOpen] = useState(false);
	const [isValid, setIsValid] = useState(false);

	const [transaction, setTransaction] = useState(undefined);
	const [returnedTransaction, setReturnedTransaction] = useState<
		undefined | Record<string, any>
	>(undefined);

	const handleValidStateChange = (valid: boolean, tx: any) => {
		setIsValid(valid);
		setReturnedTransaction(tx);
	};
	const closeWidget = () => {
		setTransaction(transaction);
		setIsOpen(false);
	};

	const onSend = (e: any) => {
		e.preventDefault();
		const { transactionAsset, beneficiaryAccountNumber, transactionAmount } =
			e.target.elements;
		setTransaction((prev: any) => ({
			...prev,
			transactionAsset: transactionAsset.value,
			beneficiaryAccountNumber: beneficiaryAccountNumber.value,
			transactionAmount: transactionAmount.value,
		}));

		setIsOpen(true);
	};

	return (
		<div className={styles.container}>
			<Head>
				<title>Notabene Widget NextJS Example</title>
				<link rel="icon" href="/favicon.ico" />
			</Head>

			<style>
				{`
				input {
					margin-bottom: 10px;
					min-width: 300px;
				}
				form {
					display: flex;
					flex-direction: column;
					align-items: center;
				}
				`}
			</style>
			<main className={styles.main}>
				<h1>Notabene Widget NextJS Example</h1>
				<form onSubmit={onSend}>
					<label htmlFor="transactionAsset">Asset</label>
					<input
						defaultValue={defaultTransaction.transactionAsset}
						type="text"
						id="transactionAsset"
					/>
					<label htmlFor="transactionAmount">Amount</label>
					<input
						defaultValue={defaultTransaction.transactionAmount}
						type="text"
						id="transactionAmount"
					/>
					<label htmlFor="beneficiaryAccountNumber">Destination</label>
					<input
						defaultValue={defaultTransaction.beneficiaryAccountNumber}
						type="text"
						id="beneficiaryAccountNumber"
					/>
					<button type="submit">SEND</button>
				</form>
				<Modal onClose={closeWidget} isOpen={isOpen}>
					<Widget
						handleValidStateChange={handleValidStateChange}
						customerToken={customerToken}
						vaspDID={vaspDID}
						widgetBaseURL={widgetBaseURL}
						transaction={transaction}
					/>
					{isValid && (
						<div>
							<h2>Transaction is valid</h2>
							<button
								onClick={() => {
									alert(
										"Ready to create tx using the following data: \n " +
											JSON.stringify(returnedTransaction, null, 2)
									);
									closeWidget();
								}}
							>
								SEND TRANSACTION
							</button>
						</div>
					)}
				</Modal>
			</main>
		</div>
	);
};

export async function getServerSideProps() {
	const { API_URL, API_TOKEN, VASP_DID, WIDGET_BASE } = process.env;

	if (!API_URL || !API_TOKEN || !VASP_DID || !WIDGET_BASE) {
		throw new Error("Missing environment variables");
	}

	// Exchange API token for customer token
	const token = await getCustomerToken(API_URL, API_TOKEN, VASP_DID);
	return {
		props: {
			customerToken: token,
			vaspDID: VASP_DID,
			widgetBaseURL: WIDGET_BASE,
		},
	};
}

export default Home;
