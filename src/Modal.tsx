import { ReactNode } from "react";

export default function Modal({
	children,
	isOpen = false,
	onClose,
}: {
	children: ReactNode | ReactNode[];
	isOpen?: boolean;
	onClose: () => void;
}) {
	return (
		<div
			style={{
				display: isOpen ? "flex" : "none",
				position: "fixed",
				width: "100%",
				justifyContent: "center",
				alignItems: "center",
				height: "100%",
				backgroundColor: "rgba(0,0,0,0.5)",
				top: 0,
				left: 0,
			}}
		>
			<div
				style={{
					minWidth: 400,
					backgroundColor: "white",
					borderRadius: 8,
					display: "flex",
					flexDirection: "column",
					alignItems: "center",
					justifyContent: "center",
					padding: 16,
					position: "relative",
					boxShadow: "0px 0px 10px 0px rgba(0,0,0,0.5)",
				}}
			>
				<button onClick={onClose} style={{ position: "relative", width: 200 }}>
					CLOSE
				</button>
				{children}
			</div>
		</div>
	);
}
