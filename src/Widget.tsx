import { useEffect, useRef, useState } from "react";
import React from "react";

interface Transaction {
	transactionAmount: string;
	transactionAsset: string;
	beneficiaryAccountNumber: string;
}
interface Props {
	customerToken: string;
	vaspDID: string;
	widgetBaseURL: string;
	transaction?: Transaction;
	handleValidStateChange: (valid: boolean, tx: Transaction) => void;
}

interface Notabene {
	setTransaction: (transaction: Transaction) => void;
	tx: Transaction;
	destroyWidget: () => void;
}

export default function Widget({
	customerToken,
	vaspDID,
	widgetBaseURL,
	transaction,
	handleValidStateChange,
}: Props) {
	// Since react 18 useEffect will be called twice. Saving render state would prevent rendering twice.
	const rendered = useRef(false);
	const notabene = useRef<null | Notabene>(null);

	const { transactionAmount, transactionAsset, beneficiaryAccountNumber } =
		(transaction || {}) as Transaction;

	const onValidStateChange = (valid: boolean) => {
		if (notabene.current?.tx)
			handleValidStateChange(valid, notabene.current.tx);
	};

	useEffect(() => {
		const render = async () => {
			// Avoid rendering twice and without window available.
			if (!window || rendered.current) {
				return;
			}
			rendered.current = true;

			// Import dynamically to avoid window from
			const { Notabene } = await import(
				"@notabene/javascript-sdk/dist/lib/index"
			);

			const nb = new Notabene({
				vaspDID: vaspDID,
				widget: widgetBaseURL,
				container: "#container",
				authToken: customerToken,
				onValidStateChange,
				transactionTypeAllowed: "ALL", // 'ALL', 'SELF_TRANSACTION_ONLY', 'VASP_2_VASP_ONLY'
				nonCustodialDeclarationType: "DECLARATION", // 'SIGNATURE', 'DECLARATION'
			});

			nb.setTransaction({
				transactionAsset,
				transactionAmount,
				beneficiaryAccountNumber,
			});

			notabene.current = nb;

			nb.renderWidget();
		};
		if (!transaction) return;
		render();

		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [transaction]);

	// Destroy widget only when component the is removed.
	useEffect(() => {
		return () => {
			if (!notabene.current) return;
			notabene.current.destroyWidget();
			rendered.current = false;
		};
	}, []);

	// Calling setTransaction trigger a reload in the widget.
	useEffect(() => {
		if (!notabene.current) return;
		notabene.current.setTransaction({
			transactionAsset,
			transactionAmount,
			beneficiaryAccountNumber,
		});
	}, [beneficiaryAccountNumber, transactionAmount, transactionAsset]);

	if (!transaction) return <></>;

	return (
		<>
			<style>
				{`
				#container div {
					width: 100% !important;
					height: 100%;
				}

				#container {
					position: relative;
					width: 100%;
					height: 100%;
				}
			`}
			</style>
			<div id="container" />
		</>
	);
}
